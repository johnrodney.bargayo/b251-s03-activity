class Camper():
	def  __init__(self, name, batch, course_type):
		self.name = name
		self.batch = batch
		self.course_type = course_type

		
	def career_track(self):
		print(f"Camper Name: {self.name}")
		print(f"Camper Batch: {self.batch}")
		print(f"Camper Course: {self.course_type}")
		

zuiit_camper = Camper("John Rodney", "251", "Python short course")

zuiit_camper.career_track()

print(f"My name is {zuiit_camper.name} of batch {zuiit_camper.batch}")
print(f"Currently enrolled in the {zuiit_camper.course_type}")